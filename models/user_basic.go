package models

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
)

type UserBasic struct {
	Identity  string `bson:"identity"`
	Account   string `bson:"account"`
	Password  string `bson:"password"`
	Nickname  string `bson:"nickname"`
	Sex       int    `bson:"sex"`
	Email     string `bson:"email"`
	Avatar    string `bson:"avatar"`
	CreatedAt int64  `bson:"created_at"`
	UpdatedAt int64  `bson:"updated_at"`
}

func (UserBasic) CollectionName() string {
	return "user_basic"
}

func GetUserBasicByAccountPassword(account, password string) (UserBasic, error) {
	var user UserBasic
	err := MongoDB.Collection(UserBasic{}.CollectionName()).FindOne(context.Background(), bson.D{{"account", account}, {"password", password}}).Decode(&user)
	return user, err
}

func GetUserBasicByIdentity(identity string) (UserBasic, error) {
	var user UserBasic
	err := MongoDB.Collection(UserBasic{}.CollectionName()).
		FindOne(context.Background(), bson.D{{"identity", identity}}).
		Decode(&user)
	return user, err
}

//查询这个email的用户数量
func GetUserBasicCountByEmail(email string) (int64, error) {
	return MongoDB.Collection(UserBasic{}.CollectionName()).
		CountDocuments(context.Background(), bson.D{{"email", email}})
}

func GetUserBasicCountByAccount(account string) (int64, error) {
	return MongoDB.Collection(UserBasic{}.CollectionName()).CountDocuments(context.Background(), bson.D{{"account", account}})
}

func InsertUserBasic(ub *UserBasic) error {
	_, err := MongoDB.Collection(UserBasic{}.CollectionName()).InsertOne(context.Background(), ub)
	return err
}

func GetUserBasicByAccount(account string) (UserBasic, error) {
	var user UserBasic
	err := MongoDB.Collection(UserBasic{}.CollectionName()).FindOne(context.Background(), bson.D{{"account", account}}).Decode(&user)
	return user, err
}
