package utils

import (
	"errors"
	"github.com/golang-jwt/jwt/v5"
	"time"
)

var JwtKey = []byte("goIM") //用于生成token命令的，可写在config配置文件中

type MyClaims struct {
	Identity             string `json:"identity"`
	Email                string `json:"email"`
	jwt.RegisteredClaims        // 内嵌标准的声明
}

//生成token
func CreateToken(identity string, email string) (string, error) {
	claim := MyClaims{
		Identity: identity,
		Email:    email,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * 24 * 7)), //到期时间
			Issuer:    "sishi",                                                //签发token的人
			IssuedAt:  jwt.NewNumericDate(time.Now()),                         // 签发时间
			NotBefore: jwt.NewNumericDate(time.Now()),                         // 生效时间
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim) // 使用HS256算法 ；返回一个指向token的指针
	return token.SignedString(JwtKey)                         //SignedString创建并返回一个完整的签名JWT。
}

//验证token
func VerifyToken(tokenString string) (*MyClaims, error) {
	token, err := jwt.ParseWithClaims(tokenString, &MyClaims{}, func(token *jwt.Token) (interface{}, error) {
		return JwtKey, nil
	}) //解析tokenString成指向token的指针

	if err != nil {
		return nil, err
	}
	// 对token对象中的Claim进行类型断言
	if claims, ok := token.Claims.(*MyClaims); ok && token.Valid {
		return claims, nil
	}
	return nil, errors.New("token验证错误")
}
