package utils

import (
	"crypto/md5"
	"fmt"
	"golang.org/x/crypto/bcrypt"
)

//密码加密,同样的密码，加密结果不一样
func ScryptPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

//用于登录密码比对
func ComparePassword(hashPassword, inputPassword string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashPassword), []byte(inputPassword))
	if err != nil {
		return false
	}
	return true
}

//md5简单加密方式
func Md5(s string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(s)))
}
