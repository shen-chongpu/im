package middleware

import (
	"github.com/gin-gonic/gin"
	"im/utils"
	"net/http"
)

func AuthCheck() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.GetHeader("token")
		claims, err := utils.VerifyToken(token)
		if err != nil {
			//如果token验证失败，就终止
			c.Abort()
			c.JSON(http.StatusOK, gin.H{
				"code": -1,
				"msg":  "用户认证不通过",
			})
			return
		}
		c.Set("user_claims", claims)
		c.Next()
	}
}
