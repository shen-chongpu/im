package test

import (
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"testing"
)

var addr = flag.String("addr", "localhost:8080", "http service address")

var upgrader = websocket.Upgrader{} // use default options

//相当于一个websocket连接的集合
var ws = make(map[*websocket.Conn]struct{})

func echo(w http.ResponseWriter, r *http.Request) {
	//升级http为websocket
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer conn.Close()
	//每一次连接都将连接加入到集合中
	ws[conn] = struct{}{}
	//添加一个关闭事件的监听器,如果断开了连接，则将连接从map中移除
	conn.SetCloseHandler(func(code int, text string) error {
		log.Printf("Connection closed: %d %s\n", code, text)
		//从ws集合中删除该连接
		delete(ws, conn)
		return nil
	})
	for {
		//接收到消息
		mt, message, err := conn.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			break
		}
		fmt.Println(mt)
		fmt.Println(string(message))
		//将接收到的信息广播给所有连接
		for c := range ws {
			err = c.WriteMessage(mt, message)
			if err != nil {
				log.Println("write:", err)
				break
			}
		}
	}
}

func TestWebsocketServer(t *testing.T) {
	http.HandleFunc("/echo", echo)
	log.Fatal(http.ListenAndServe(*addr, nil))
}

func TestGinWebsocketServer(t *testing.T) {
	r := gin.Default()
	r.GET("echo", func(ctx *gin.Context) {
		echo(ctx.Writer, ctx.Request)
	})
	r.Run()
}
