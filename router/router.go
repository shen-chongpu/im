package router

import (
	"github.com/gin-gonic/gin"
	"im/middleware"
	"im/service"
)

func Router() *gin.Engine {
	r := gin.Default()

	r.POST("/login", service.Login)
	r.POST("/send_code", service.SendCode)
	r.POST("/register", service.Register)
	auth := r.Group("/u", middleware.AuthCheck())
	{
		auth.GET("/user/detail", service.UserDetail)             //个人信息
		auth.GET("/user/query", service.UserQuery)               //查询指定用户信息
		auth.GET("/websocket/message", service.WebsocketMessage) //发送接收消息
		auth.GET("/chat/list", service.ChatList)                 //聊天记录
		auth.POST("/user/friend", service.UserAddFriend)         //添加好友
		auth.DELETE("/user/friend", service.UserDeleteFridend)

		auth.POST("/room", service.CreateRoom)    //创建群聊
		auth.POST("/into_room", service.IntoRoom) //加入群聊
	}
	return r
}
